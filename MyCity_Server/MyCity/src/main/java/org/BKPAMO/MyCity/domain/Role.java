package org.BKPAMO.MyCity.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by AleM on 24/03/15.
 */
@Entity
public class Role {
    @Id
    private String role;

    @JsonIgnore
    //@JsonBackReference
    @OneToMany(mappedBy = "role")
    private Set<Authorities> authorities = new HashSet<Authorities>();

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<Authorities> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authorities> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String toString() {
        return "Role{" +
                "role='" + role + '\'' +
                ", authorities=" + authorities +
                '}';
    }
}
