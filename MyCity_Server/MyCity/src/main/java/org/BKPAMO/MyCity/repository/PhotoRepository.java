package org.BKPAMO.MyCity.repository;

import org.BKPAMO.MyCity.domain.Incidence;
import org.BKPAMO.MyCity.domain.Photo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by AleM on 12/02/15.
 */
@Repository
public interface PhotoRepository extends PagingAndSortingRepository<Photo, Long> {
}
