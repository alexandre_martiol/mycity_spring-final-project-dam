package org.BKPAMO.MyCity.repository;

import org.BKPAMO.MyCity.domain.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by AleM on 12/02/15.
 */
@Repository
public interface AdminRepository extends CrudRepository<Admin, Long> {
}
