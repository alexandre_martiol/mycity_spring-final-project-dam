package org.BKPAMO.MyCity.repository;

import org.BKPAMO.MyCity.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;

/**
 * Created by AleM on 12/02/15.
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    public User findByUsernameAndPassword(@Param("username") String username,
                                      @Param("password") String password);

    public User findByUsername(@Param("username") String username);
}
