package org.BKPAMO.MyCity.repository;

import org.BKPAMO.MyCity.domain.Town;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by AleM on 12/02/15.
 */
@Repository
public interface TownRepository extends PagingAndSortingRepository<Town, Long> {
    public Town findByName(@Param("name") String name);
}
