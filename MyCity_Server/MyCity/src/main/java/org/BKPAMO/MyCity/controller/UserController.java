package org.BKPAMO.MyCity.controller;

import org.BKPAMO.MyCity.domain.Authorities;
import org.BKPAMO.MyCity.domain.Role;
import org.BKPAMO.MyCity.domain.User;
import org.BKPAMO.MyCity.repository.AuthoritiesRepository;
import org.BKPAMO.MyCity.repository.RoleRepository;
import org.BKPAMO.MyCity.repository.UserRepository;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by AleM on 17/02/15.
 */

@RestController
@ExposesResourceFor(User.class)
@RequestMapping("/user")
@Transactional
public class UserController {
    @Resource(name = "userRepository")
    private UserRepository userRepository;

    @Resource(name = "authoritiesRepository")
    private AuthoritiesRepository authoritiesRepository;

    @Resource(name = "roleRepository")
    private RoleRepository roleRepository;

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    @ResponseBody
    public void registerUser(@RequestBody User user) {
        String passEncrypt = this.getHashPassword(user.getPassword());
        user.setPassword(passEncrypt);

        Role role = roleRepository.findByRole("ROLE_ADMIN");

        userRepository.save(user);

        Authorities newAuthority = new Authorities();
        newAuthority.setRole(role);
        newAuthority.setUser(user);
        authoritiesRepository.save(newAuthority);

        System.out.println("NEW USER WITH USERNAME: " + user.getUsername() + " HAS BEEN REGISTERED CORRECTLY!");
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public User findByUsernameAndPass(@RequestParam(value = "username") String username,
                                      @RequestParam(value = "password") String password) {
        User user = new User();

        if(checkIfPasswordIsCorrect(username, password)) {
            user = userRepository.findByUsername(username);

            System.out.println("USER WITH USERNAME: " + user.getUsername() + " HAS BEEN LOGGED IN CORRECTLY!");
        }

        return user;
    }

    @RequestMapping(method = RequestMethod.GET, params = "username")
    @ResponseBody
    public User findByUsername(@RequestParam(value = "username") String username) {
        User user = userRepository.findByUsername(username);

        return user;
    }

    private boolean checkIfPasswordIsCorrect(String username, String password) {
        User user = userRepository.findByUsername(username);

        if (user != null) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

            return encoder.matches(password, user.getPassword());
        }

        return false;
    }

    private String getHashPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);

        return hashedPassword;
    }
}
