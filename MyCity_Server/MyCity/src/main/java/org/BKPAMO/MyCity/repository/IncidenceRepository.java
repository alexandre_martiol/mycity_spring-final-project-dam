package org.BKPAMO.MyCity.repository;

import org.BKPAMO.MyCity.domain.Incidence;
import org.BKPAMO.MyCity.domain.Town;
import org.BKPAMO.MyCity.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by AleM on 12/02/15.
 */
@Repository
public interface IncidenceRepository extends PagingAndSortingRepository<Incidence, Long> {
    public Incidence[] findByTown(@Param("town") Town town);
    public Incidence[] findByStatus(@Param("status") int status);
    public Incidence findById(@Param("id") Long id);
    public Incidence[] findByUser(@Param("user") User user);
}
