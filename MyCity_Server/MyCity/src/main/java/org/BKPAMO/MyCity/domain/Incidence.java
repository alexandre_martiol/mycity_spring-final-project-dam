package org.BKPAMO.MyCity.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by AleM on 02/02/15.
 */

@Entity
public class Incidence {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    //@JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    private String description;

    @Size(max = 50, message = "Address must have <50 characters")
    private String address;

    @DateTimeFormat
    private Date date;

    @Max(1)
    private int status;

    //@JsonBackReference
    @OneToMany(mappedBy = "incidence")
    private List<Photo> photos = new ArrayList<>();

    private String latitudeCoordinate;

    private String longitudeCoordinate;

    @Max(8)
    private int type;

    @JsonIgnore
    //@JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Town town;


    //Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLatitudeCoordinate() {
        return latitudeCoordinate;
    }

    public void setLatitudeCoordinate(String latitudeCoordinate) {
        this.latitudeCoordinate = latitudeCoordinate;
    }

    public String getLongitudeCoordinate() {
        return longitudeCoordinate;
    }

    public void setLongitudeCoordinate(String longitudeCoordinate) {
        this.longitudeCoordinate = longitudeCoordinate;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    @Override
    public String toString() {
        return "Incidence{" +
                "id=" + id +
                ", user=" + user +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", date=" + date +
                ", status=" + status +
                ", photos=" + photos +
                ", latitudeCoordinate='" + latitudeCoordinate + '\'' +
                ", longitudeCoordinate='" + longitudeCoordinate + '\'' +
                ", type=" + type +
                ", town=" + town +
                '}';
    }
}

