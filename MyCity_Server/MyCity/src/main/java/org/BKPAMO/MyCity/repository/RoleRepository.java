package org.BKPAMO.MyCity.repository;

import org.BKPAMO.MyCity.domain.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by AleM on 24/03/15.
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
    public Role findByRole(@Param("role") String role);
}
