package org.BKPAMO.MyCity.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * Created by AleM on 02/02/15.
 */

@Entity
public class Photo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    //@JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Incidence incidence;

    private int numPhoto;

    //Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumPhoto() {
        return numPhoto;
    }

    public void setNumPhoto(int numPhoto) {
        this.numPhoto = numPhoto;
    }

    public Incidence getIncidence() {
        return incidence;
    }

    public void setIncidence(Incidence incidence) {
        this.incidence = incidence;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", incidence=" + incidence +
                ", numPhoto=" + numPhoto +
                '}';
    }
}