package org.BKPAMO.MyCity.repository;

import org.BKPAMO.MyCity.domain.Authorities;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by AleM on 13/03/15.
 */
@Repository
public interface AuthoritiesRepository extends PagingAndSortingRepository<Authorities, Long> {
    public Authorities findByUserId(@Param("id") Long id);
}
