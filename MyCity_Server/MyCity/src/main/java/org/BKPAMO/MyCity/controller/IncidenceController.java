package org.BKPAMO.MyCity.controller;

import org.BKPAMO.MyCity.domain.Incidence;
import org.BKPAMO.MyCity.domain.Town;
import org.BKPAMO.MyCity.domain.User;
import org.BKPAMO.MyCity.repository.*;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.transaction.Transactional;

@RestController
@ExposesResourceFor(User.class)
@RequestMapping("/incidence")
@Transactional
public class IncidenceController {

    @Resource(name = "incidenceRepository")
    private IncidenceRepository incidenceRepository;

    @Resource(name = "userRepository")
    private UserRepository userRepository;

    @Resource(name = "townRepository")
    private TownRepository townRepository;

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Incidence registerIncidence(@RequestBody Incidence incidence) {
        //First take the name of the user and add the user to the incidence.
        //It is used the address var beacuse is the only option.
        User user = userRepository.findByUsername(incidence.getAddress());

        incidence.setUser(user);
        incidence.setAddress("");

        Town town = townRepository.findByName("Barcelona");
        incidence.setTown(town);

        incidenceRepository.save(incidence);

        System.out.println("THE INCIDENCE HAS BEEN UPLOADED CORRECTLY!");

        return incidence;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public void updateIncidence(@RequestBody Incidence incidence) {

    }

    @RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
    @ResponseBody
    public void updateStatusIncidence(@RequestParam(value = "status") int status,
                                      @RequestParam(value = "incidenceId") long id) {
        Incidence incidence = incidenceRepository.findById(id);

        incidence.setStatus(status);

        incidenceRepository.save(incidence);
    }

    @RequestMapping(method = RequestMethod.GET, params = "id")
    @ResponseBody
    public Incidence findById(@RequestParam(value = "id") Long id) {

        Incidence incidence = incidenceRepository.findById(id);

        return incidence;
    }

    @RequestMapping(method = RequestMethod.GET, params = "town")
    @ResponseBody
    public Incidence[] findByTown(@RequestParam(value = "town") Town town) {

        Incidence[] incidence = incidenceRepository.findByTown(town);

        return incidence;
    }

    @RequestMapping(method = RequestMethod.GET, params = "status")
    @ResponseBody
    public Incidence[] findByStatus(@RequestParam(value = "status") int status) {

        Incidence[] incidence = incidenceRepository.findByStatus(status);

        return incidence;
    }

    @RequestMapping(method = RequestMethod.GET, params = "user")
    @ResponseBody
    public Incidence[] findByUser(@RequestParam(value = "user") User user) {

        Incidence[] incidence = incidenceRepository.findByUser(user);

        return incidence;
    }
}
