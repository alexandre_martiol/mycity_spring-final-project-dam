INSERT INTO `MyCity`.`role` (`role`) VALUES ('ROLE_ADMIN');

INSERT INTO `user` (`id`, `email`, `enabled`, `lastname`, `name`, `password`, `points`, `sex`, `username`) VALUES
  (1, 'aeeel@gmail.com', 1, 'Martinez', 'Alex', '$2a$10$QY7PCiVHjaZRekc2McLPLO/kqUtwPUP9G4uP5BFS.QzWLkSnZwOWW', 0, 1, 'Alexito');

INSERT INTO `authorities`(`id`, `authority`, `username`) VALUES (1,'ROLE_ADMIN','Alexito');

INSERT INTO `town` (`id`, `name`) VALUES (875, 'Barcelona');

INSERT INTO `incidence`(`id`, `date`, `description`, `latitude_coordinate`, `longitude_coordinate`, `status`, `type`, `town_id`, `user_id`) VALUES
  (1,'2015/01/01','Se ha roto una cañeria en medio de la calle y el agua no para de salir. Esta entrando en diferentes comercios y dificulta el paso de los coches', '41.397765', '2.171620', 0, 1, 875, 1);

INSERT INTO `incidence`(`id`, `date`, `description`, `latitude_coordinate`, `longitude_coordinate`, `status`, `type`, `town_id`, `user_id`) VALUES
  (2,'2015/05/18','Unos chavales rompieron varios semaforos de la calle y ahora los peatones no podemos cruzar con seguridad', '41.385085', '2.164198', 1, 4, 875, 1);

INSERT INTO `incidence`(`id`, `date`, `description`, `latitude_coordinate`, `longitude_coordinate`, `status`, `type`, `town_id`, `user_id`) VALUES
  (3,'2015/05/18','Las farolas de la calle no funcionan y no se ve nada por la noche. Varios vecinos tenemos miedo a salir de casa por la noche.', '41.379874', '2.161504', 2, 7, 875, 1);