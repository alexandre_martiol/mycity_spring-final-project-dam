
function init(){
    $('.clck').click(function(){
        showIncidenceInfo($(this).attr('alt'));
    });
}

function showIncidenceInfo(incidence){

    $.ajax({
        type: "GET",
        url: "http://localhost:8080/incidence",
        dataType: "json",
        data: {id: incidence},
        success: function (respJSON) {
            console.log(respJSON);
            if (respJSON != null) {
                var incidence = respJSON;
                if (respJSON.status == 0){
                    console.log("status 0");
                    $.get('templates/infoWindowRed.html', function(data){
                        fillIncidenceInfo(data,incidence);
                    });
                }else if(respJSON.status == 1){
                    console.log("status 1");
                    $.get('templates/infoWindowOrange.html', function(data){
                        fillIncidenceInfo(data,incidence);
                    });
                }else{
                    console.log("status 1");
                    $.get('templates/infoWindowGreen.html', function(data){
                        fillIncidenceInfo(data,incidence);
                    });
                }
            }
            else {
                return false;
            }
        }
    });

}

function fillIncidenceInfo(template,incidence){
    console.log(template);
    console.log(incidence);
    var temp = template;
    temp = temp.replace("[@Image]", "");
    temp = temp.replace("[@Address]", "");

    var d = new Date(incidence.date);
    temp = temp.replace("[@Date]", ""+ d.getFullYear()+"/"+(d.getMonth()+1)+"/"+ d.getDate());

    temp = temp.replace("[@Type]", incidence.type);
    temp = temp.replace("[@Description]", incidence.description);
    temp = temp.replace("[@Id]", incidence.id);


    $('body').append('<div class="blk"/>');
    $('body').append(temp);
    $('.blk').click(hideIncidenceInfo);
}

function hideIncidenceInfo(){
    $('.blk').remove();
    $('#info').remove();
}