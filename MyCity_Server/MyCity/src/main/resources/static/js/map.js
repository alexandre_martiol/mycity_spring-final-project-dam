$(document).ready(loadMap);

function loadMap(){
    $('#map-body').gmap3({
        map:{
            options:{
                zoom: 15
            }
        },
        getgeoloc: {
            callback: function (latLng) {
                if (latLng) {
                    $(this).gmap3({
                        marker: {
                            latLng: latLng
                        },

                        map: {
                            options: {
                                center:latLng
                            }
                        }
                    });
                }
            }
        }
    });
}

