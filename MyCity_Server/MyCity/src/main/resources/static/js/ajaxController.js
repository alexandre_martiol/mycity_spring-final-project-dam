$(document).ready(incidenceRequest);

function incidenceRequest() {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/incidence",
        dataType: "json",
        data: {town: 875},
        success: function (respJSON) {
            if (respJSON.length > 0) {
                var incidences = respJSON;
                for (var i = 0; i < respJSON.length; i++) {
                    $("#map-body").gmap3({
                        marker: {
                            values: [
                                {latLng: [respJSON[i].latitudeCoordinate, respJSON[i].longitudeCoordinate], id: respJSON[i].id}
                            ],
                            options: {
                                draggable: false
                            },
                            events: {
                                click: function(e){
                                    showIncidenceInfo(e.incidence);
                                }
                            }
                        }
                    });
                    var marker = $("#map-body").gmap3({
                        get: {
                            name: "marker",
                            last: true
                        }});
                    marker.set('incidence',respJSON[i].id);
                }
                $.get('templates/incidence.html', function(data){
                    loadIncidences(data,incidences);
                });
                init();
            }
            else {
                return false;
            }
        }
    });
    return false;
}

function loadIncidences(template, incidences){
    for (var i = 0; i < incidences.length; i++) {

        var temp = template;
        temp = temp.replace("[@Image]", "");
        temp = temp.replace("[@Id]", incidences[i].id);

        var inci = "error";
        switch (incidences[i].type){
            case 1:
                inci = "Inundación";
                break;
            case 2:
                inci = "Difícil acceso para minusválidos";
                break;
            case 3:
                inci = "Vegetación en mal estado";
                break;
            case 4:
                inci = "Falta de señalización";
                break;
            case 5:
                inci = "Acumulación de desechos";
                break;
            case 6:
                inci = "Desprendimientos";
                break;
            case 7:
                inci = "Falta de iluminación";
                break;
            case 8:
                inci = "Desperfectos u objetos en calzada";
                break;
            default:
                inci = "General";
        }

        temp = temp.replace("[@Incidence]", inci);
        temp = temp.replace("[@Type]", incidences[i].type);

        var d = new Date(incidences[i].date);
        temp = temp.replace("[@Date]", ""+ d.getFullYear()+"/"+(d.getMonth()+1)+"/"+ d.getDate());

        var stat;
        switch (incidences[i].status){
            case 1:
                stat = "orange";
                break;
            case 2:
                stat = "green";
                break;
            default:
                stat = "red";
        }
        temp = temp.replace("[@Status]", stat);

        $('#incidence-list').append(temp);
    }
    init();
}

function deleteIncidence(){

}

function modifyIncidenceStatus(status,id){
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/incidence/updateStatus",
        dataType: "json",
        data: {status: status,id: id},
        success: function (respJSON) {
            console.log(respJSON);
        }
    });
}

